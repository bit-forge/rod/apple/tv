//
//  rodApp.swift
//  rod
//
//  Created by Alex on 03.04.2024.
//

import SwiftUI

@main
struct rodApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
